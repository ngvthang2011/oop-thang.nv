<?php
require_once('./../abstract/BaseDao.php');

class AccessotionDao extends BaseDao
{
    /**
     * get Accessotion by Name
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name)
    {
        return $this->database->getTableByName('accessotionTable', $name);
    }

    /**
     * get Accessotion where
     * @return mixed
     */
    public function search()
    {
        return $this->database->selectTable('accessotionTable');
    }
}