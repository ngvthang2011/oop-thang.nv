<?php

class Database
{
    public $productTable = array();
    public $categoryTable = array();
    public $accessotionTable = array();
    protected static $instants = null;

    /**
     * get Database instant
     * @param string $className
     * @return Database $instants
     */
    public static function getInstants($className = 'Database')
    {
        if(empty(self::$instants))
        {
            return self::$instants = new $className();
        }
        return self::$instants;
    }

    /**
     * insert row to table
     * @param string $name
     * @param object $row
     * @return bolean
     */
    public function insertTable(string $name, object $row)
    {
        if(is_subclass_of($row, 'BaseRow'))
        {
            $this->{$name}[] = $row;
            return 1;
        }

        return 0;
    }

    /**
     * select all row or by ID in table
     * @param string $name
     * @param int $whereId
     * @return mixed
     */
    public function selectTable(string $name, int $whereId = null)
    {
        if(!empty($whereId))
        {
            foreach($this->{$name} as $value)
            {
                if($value->getId() == $whereId)
                {
                    return $value;
                }
            }
        }
        return $this->{$name};
    }

    /**
     * Update row on table by ID
     * @param string $name
     * @param object $row
     * @return bolean
     */
    public function updateTable(string $name, object $row)
    {
        if(is_subclass_of($row, 'BaseRow'))
        {
            foreach ($this->{$name} as $key=>$value)
            {
                if($this->{$name}[$key]->getId() == $row->getId())
                {
                    $this->{$name}[$key] = $row;
                }
            }
            return 1;
        }

        return 0;
    }

    /**
     * Delete row on table by ID
     * @param string $name
     * @param object $row
     * @return bolean
     */
    public function deleteTable(string $name, object $row)
    {
        if(is_subclass_of($row, 'BaseRow'))
        {
            foreach ($this->{$name} as $key=>$value)
            {
                if($this->{$name}[$key]->getId() == $row->getId())
                {
                    unset($this->{$name}[$key]);
                }
            }
            return 1;
        }
        return 0;
    }

    /**
     * Delete all row on table
     * @param string $name
     * @return void
     */
    public function truncateTable(string $name)
    {
        $this->{$name} = null;
    }

    /**
     * Update row on table by ID
     * @param string $name
     * @param object $row
     * @return bolean
     */
    public function updateTableById(int $id, object $row)
    {
        if(is_subclass_of($row, 'BaseRow'))
        {
            switch(get_class($row))
            {
                case 'Product' :
                    foreach($this->productTable as $key=>$product)
                    {
                        if($product->getId() == $id)
                        {
                            $this->productTable[$key] = $row;
                        }
                    }
                    break;
                case 'Category' :
                    foreach($this->categoryTable as $key=>$category)
                    {
                        if($category->getId() == $id)
                        {
                            $this->categoryTable[$key] = $row;
                        }
                    }
                    break;
                case 'Accessotion' :
                    foreach($this->accessotionTable as $key=>$accessotion)
                    {
                        if($accessotion->getId() == $id)
                        {
                            $this->accessotionTable[$key] = $row;
                        }
                    }
                    break;
            }
            return 1;
        }
        return 0;
    }

    /**
     * Get row on table by Name
     * @param string $nameTable
     * @param string $name
     * @return mixed
     */
    public function getTableByName(string $nameTable, string $name)
    {
        foreach($this->{$nameTable} as $row)
        {
            if($row->getName() == $name)
            {
                return $row;
            }
        }
        return 0;
    }

}