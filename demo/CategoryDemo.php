<?php
require_once('./../entity/Category.php');

class CategoryDemo extends Category
{
    public function __construct()
    {
       
    }

    /**
     * Create instants Category
     * @param int $id
     * @param string $name
     * return mixed
     */
    public function createCategoryTest(int $id, string $name)
    {
        $category =  new Category($id, $name);
        return $category;
    }

    /**
     * Print Category
     * @param Category $category
     * return mixed
     */
    public function printCategory(Category $category)
    {
        echo 'Category ID: '.$category->getId().'<br/>';
        return $category;
    }
}
